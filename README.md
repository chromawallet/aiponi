# Contents

1. **Running a single Aiponi node (development)**
2. **Running Aiponi on the blockchain (production)**

# Running a single Aiponi node

## Prerequisites
1.  **Java 8**
    * Postchain runs on [Java 8](http://www.oracle.com/technetwork/java/javase/downloads/jdk8-downloads-2133151.html).
    * Make sure `java -version` displays Java 8 as your version.
2. **PostgreSQL**
    * Install [PostgreSQL](https://www.postgresql.org/download/).
2. **NodeJS**
    * Install [NodeJS](https://nodejs.org/en/download/).
## Set up

Set up PostgreSQL for Postchain:

On MacOS:
```
sudo -u _postgres -i
```
On Linux:
```
sudo -u postgres -i
```
Then:
```
createdb postchain
psql -c "create role postchain LOGIN ENCRYPTED PASSWORD 'postchain'" -d postchain
psql -c "grant ALL ON DATABASE postchain TO postchain" -d postchain
sudo exit
```

## Run Postchain Node

Run the Postchain node:

`./postchain.sh -j aiponi-1.0-SNAPSHOT.jar -i 0 -c single.properties`

There might be some warnings at the start, ignore those.

## Send Transactions

To send transactions from example phone payment logs stored in client/logs.csv to the Postchain node:
```
cd client
node index
```
Make sure your postchain node is running when you send the transactions.

## Explore Postchain Data

To display the PostgreSQL tables associated with the Postchain node:
```
psql "user=postchain password=postchain dbname=postchain host=127.0.0.1"
SET search_path TO etk;
\dt
```

To show Postchain block data (encoded with ASN.1):
```
SELECT * FROM blocks;
```

To show log data inserted into PostgreSQL RDB:
```
SELECT * FROM phone_payments;
```

# Running Aiponi on the blockchain

Now, instead of running Aiponi with a single node, multiple nodes will be signing and validating transactions before they are accepted into Aiponi. BFT is reached when at least four nodes are part of the network. Below are instructions on how to configure nodes to become a part of the Aiponi network.

## Prerequisites
1. An understanding of how Aiponi runs on Postchain. It is recommended to attemp the below once you have managed to run a single Aiponi node successfully (instructions above).

## Set up

The set up process is similar to the one used to run a single Aiponi node. Below is a recommended approach to setting up a network node.

**Network Properties**

Delete 
```
single.properties
```

And move these two property files into the root directory:
```
aiponi-multi/common.properties
aiponi-multi/private.properties
```

**Private Properties**

Open the file
```
private.properties
```

This file contains the configuration of your node. In other words, it is unique to your node. Most of the configuration can be left alone, but there are a few fields that need to be edited:
```
messaging.privkey
messaging.pubkey
blockchain.1.blocksigningprivkey
```

These fields store the public and private key of your node. To generate a new keypair, run:
```
./postchain.sh -k
```

Copy your private key into
```
messaging.privkey
blockchain.1.blocksigningprivkey
```

And your public key into
```
messaging.pubkey
```

You can also change 
```
database.schema
```
to a PostgreSQL schema name of your choice.

**Common Properties**

Open the file
```
common.properties
```

This file is shared amongst all nodes in the network and needs to be identically replicated on all nodes. In other words, once this configuration changes for one node, it needs to be broadcasted to the entire network. It stores information about where other nodes on the network are located and how they can be identified.

For every added node _n_, with index _i = n-1_, the following need to be appended to the file:

```
node.i.id
node.i.host
node.i.port
node.i.pubkey
```
* id: A plaintext identification of the node.
* host: The IP of the node.
* port: The port where Postchain is listening on the node. NB: This is actually where the port for a node is determined. So, if you are node _i_, then this is where you determine the port at which your Postchain instance will be listening.
* pubkey: The public key of the node. Must match the public key in the node's _private.properties_.

The public key of node _i_ (same as _node.i.pubkey_) must then be appended the field
```
blockchain.1.signers
```

## Running the Node

To run the node, run the following command, replacing [INDEX] with the index of your node.
```
./postchain.sh -j aiponi-1.0-SNAPSHOT.jar -i [INDEX] -c private.properties
```

Your node will now receive all the already-validated blocks from the network and validate them. Once that is done, your node is now part of the Aiponi network: it can broadcast, receive, and verify transactions on the blockchain.
